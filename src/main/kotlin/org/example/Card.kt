package org.example

import java.lang.IllegalArgumentException

enum class Suit {
    Hearts,
    Clubs,
    Diamonds,
    Spades;

    companion object {
        fun fromString(suit: String): Suit {
            if (suit.length != 1) {
                throw IllegalArgumentException("org.example.Suit $suit does not exist")
            }
            return values().find { it.name.startsWith(suit) }
                ?: throw IllegalArgumentException("org.example.Suit $suit does not exist")
        }
    }
}

sealed class Rank {
    fun numericValue(): Int =
        when (this) {
            is Number -> this.number
            is Picture -> translateRank(this.letter)
        }

    companion object {
        fun fromString(rankString: String): Rank {
            val number = rankString.toIntOrNull()
            return if (number != null) {
                Number(number)
            } else {
                Picture(rankString)
            }
        }
    }

    private fun translateRank(rank: String): Int {
        val numericValueIfPossible = rank.toIntOrNull()

        return numericValueIfPossible ?: when (rank) {
            "J" -> 11
            "Q" -> 12
            "K" -> 13
            "A" -> 14
            else -> throw IllegalArgumentException("${rank} is not a legal rank")
        }
    }

}

data class Number(val number: Int) : Rank()
data class Picture(val letter: String) : Rank()

data class Card(val rank: Rank, val suit: Suit) {
    companion object {
        fun fromString(str: String): Card {
            val rank = str.dropLast(1)
            val suit = str.takeLast(1)

            return Card(Rank.fromString(rank), Suit.fromString(suit))
        }
    }
}
