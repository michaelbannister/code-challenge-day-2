package org.example

data class Player(
    val name: String,
    val cards: List<Card>,
    val bet: Int
)
