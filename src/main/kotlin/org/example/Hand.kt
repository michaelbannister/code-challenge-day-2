package org.example

sealed class Hand
data class HighestValue(val rank: Int) : Hand()
data class FourOfAKind(val rank: Int) : Hand()
data class Pair(val rank: Int) : Hand()
data class ThreeOfAKind(val rank: Int) : Hand()

fun findBestHand(sevenCards: List<Card>): Hand {
    listOf(
        ::tryFourOfAKind,
        ::tryThreeOfAKind,
        ::tryPair
    ).forEach { rule ->
        val hand = rule(sevenCards)
        if (hand != null) return hand
    }
    return finallyHighestValue(sevenCards)
}

fun tryFourOfAKind(sevenCards: List<Card>): Hand? {
    val countByRank = sevenCards.groupBy(Card::rank). mapValues { (_, v) -> v.size }
    val rankWithCountOfFour = countByRank.findRankWithCount(4)
    return rankWithCountOfFour?.let {
        FourOfAKind(it.numericValue())
    }
}

fun tryThreeOfAKind(sevenCards: List<Card>): Hand? {
    val countByRank = sevenCards.groupBy(Card::rank).mapValues { (_, v) -> v.size }
    val rankWithCountOfFour = countByRank.findRankWithCount(3)
    return rankWithCountOfFour?.let {
        ThreeOfAKind(it.numericValue())
    }
}

fun tryPair(sevenCards: List<Card>): Hand? {
    val countByRank = sevenCards.groupBy(Card::rank).mapValues { (_, v) -> v.size }
    val rankWithCountOfFour = countByRank.findRankWithCount(2)
    return rankWithCountOfFour?.let {
        Pair(it.numericValue())
    }
}

fun finallyHighestValue(sevenCards: List<Card>): Hand {
    val maxRank = sevenCards.map { it.rank.numericValue() }.max()!!
    return HighestValue(maxRank)
}


private fun Map<Rank, Int>.findRankWithCount(count: Int) =
    entries.find { it.value == count }?.key
