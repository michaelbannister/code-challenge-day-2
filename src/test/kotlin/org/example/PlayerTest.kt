package org.example

import ch.tutteli.atrium.api.fluent.en_GB.toBe
import ch.tutteli.atrium.api.verbs.expect
import org.junit.jupiter.api.Test

class PlayerTest {

    @Test
    fun `can construct a player`() {
        val player = Player("Owen", cards("2H", "3C"), 10)
        expect(player.name).toBe("Owen")
    }

    @Test
    fun `players can be equal`() {
        val player1 = Player("Owen", cards("2H", "3C"), 10)
        val player2 = Player("Owen", cards("2H", "3C"), 10)

        expect(player1).toBe(player2)

        val player3 = player1.copy(name = "Michael")
        expect(player3.bet).toBe(10)
    }
}
