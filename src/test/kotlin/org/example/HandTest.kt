package org.example

import ch.tutteli.atrium.api.fluent.en_GB.feature
import ch.tutteli.atrium.api.fluent.en_GB.isA
import ch.tutteli.atrium.api.fluent.en_GB.toBe
import ch.tutteli.atrium.api.verbs.expect
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

class HandTests {

    @Nested
    @DisplayName("Highest value")
    class HighestValueTest {
        @Test
        fun `best hand is highest value`() {
            val hand = findBestHand(cards("2H", "3C", "4C", "5C", "7C", "8C", "9H"))
            expect(hand).isA<HighestValue>()
                .feature(HighestValue::rank).toBe(9)
        }

        @Test
        fun `best hand is highest value - check it works with picture cards`() {
            val hand = findBestHand(cards("2H", "3C", "4C", "5C", "7C", "QH", "AS"))
            expect(hand).isA<HighestValue>()
                .feature(HighestValue::rank).toBe(14)
        }

    }

    @Nested
    @DisplayName("Four of a kind")
    class FourOfAKindTest {
        @Test
        fun `best hand is four of a kind`() {
            val hand = findBestHand(cards("2H", "2D", "3D", "2S", "7C", "KH", "2C"))
            expect(hand).isA<FourOfAKind>()
                .feature(FourOfAKind::rank).toBe(2)
        }
    }

}
