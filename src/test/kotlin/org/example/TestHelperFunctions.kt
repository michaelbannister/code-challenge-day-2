package org.example

fun cards(vararg cards: String): List<Card> {
    return cards.toList().map(Card.Companion::fromString)
}
